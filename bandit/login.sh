#!/bin/bash

if [ "$1" == "" ];then
	echo "Usage: login.sh <level file name>"
	exit 1
fi
hostname="bandit.labs.overthewire.org"
username=`cat $1 | awk -F ':' '{print $1}'`
password=`cat $1 | awk -F ':' '{print $2}'`
sshpass -p $password ssh -p 2220 $username@$hostname 
