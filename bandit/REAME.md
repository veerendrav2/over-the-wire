host: bandit.labs.overthewire.org
port: 2220
username: bandit0
password: bandit0


How to connect?
-----------------
ssh -p 2220 bandit0@bandit.labs.overthewire.org

TIPS For Levels
----------------

#### Level 5 - Level 6
`find . -type f ! -perm /a=x -exec du -b {} + | grep 1033`

#### Level 6 - Level 7
`find / -type f -user bandit7 -group bandit6 2>/dev/null`

#### Level 8 - Level 9
`cat data.txt | sort | uniq -c`

#### Level 11 - Level 12
`cat data.txt | tr 'A-Za-z' 'N-ZA-Mn-za-m'`

#### Level 14 - Level 15
`echo "4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e" | nc localhost 30000`

#### Level 15 - Level 16
`openssl s_client -connect localhost:30001`

#### Level 
